import web3 from './web3';
import CampaignFactory from './build/CampaignFactory.json';

const instance = new web3.eth.Contract(
    JSON.parse(CampaignFactory.interface),
    '0xf2bB048B0B6dEB458a1A8065c44d537E03F56CDd'
);

export default instance;