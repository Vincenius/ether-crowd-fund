import React, {Component} from 'react';
import {Card, Button} from 'semantic-ui-react';
import factory from '../ethereum/factory';
import Layout from '../components/Layout';
import { Link } from '../routes';

class CampaignIndex extends Component {
    // componentDidMount() is not used because we are using next.js
    // and the props have to be defined on the server side

    static async getInitialProps() {
        const campaigns = await factory.methods.getDeployedCampaigns().call();

        return {campaigns: campaigns}; // es6 = { campaigns }
    }

    renderCampaigns() {
        const items = this.props.campaigns.map((campaign) => {
            return {
                header: campaign,
                description: (
                    <Link route={`/campaigns/${campaign}`}>
                        <a>View Campaign</a>
                    </Link>
                ),
                fluid: true
            }
        });

        return <Card.Group items={items}/>
    }

    render() {
        return (
            <Layout>
                <div>
                    <h3>Open Campaigns</h3>

                    <Link route="/campaigns/new">
                        <a>
                            <Button floated="right" content="Create Campaign" icon="add" primary/>
                        </a>
                    </Link>

                    {this.renderCampaigns()}
                </div>
            </Layout>
        )
    }
}

export default CampaignIndex;